workflow:
  # This rules avoid running duplicate pipelines
  rules:
    # Don't run when pushing to a topic, but still run when a
    # merge-request is created or updated.
    - if: $CI_COMMIT_BRANCH =~ /^topic\/.*/ && $CI_PIPELINE_SOURCE == "push"
      when: never
    - when: always

stages:
  - build
  - deploy
  - prune

collect-existing-packages:
  stage: build
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:slim
  artifacts:
    expire_in: 1 day
    paths:
      - existing-packages.txt
  before_script:
    - pip install python-gitlab
  script:
    - gitlab -v -o json --server-url https://${CI_SERVER_HOST}
         project-package list --project-id ${CI_PROJECT_ID}
         --get-all > existing-packages.txt

prune-packages:
  stage: prune
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:slim
  before_script:
    - apt > /dev/null update -qq
    - apt > /dev/null install -qq --yes jq
    - pip install python-gitlab
  script:
    - IDs=$(jq '.[].id' existing-packages.txt)
    - |
      for id in $IDs ; do
         gitlab -v --server-url https://${CI_SERVER_HOST} \
           project-package delete --project-id ${CI_PROJECT_ID} --id ${id}
      done


package:
  stage: build
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:slim
  artifacts:
    expire_in: 1 day
    paths:
      - packages
  variables:
    UPSTREAM_URL: https://foss.heptapod.net/tryton/tryton/
    BRANCH: default
  before_script:
    - apt > /dev/null update -qq
    - apt > /dev/null install -qq --yes wget bzip2
    # for some reasons directories survive job runs
    - pip install setuptools
    - rm -rf *
    # get upstream archive
    - wget --no-verbose -O tryton-branch-$BRANCH.tar.bz2
      $UPSTREAM_URL/-/archive/branch/$BRANCH/tryton-branch-$BRANCH.tar.bz2
    - tar -xj -f tryton-branch-$BRANCH.tar.bz2
    - cd tryton-branch-$BRANCH
  script:
    - NODE=$(sed -n -e '/^node:\s/ s/.* // p' .hg_archival.txt)
    - POST_VERSION=$(date +.post%s+h${NODE::7})
    - echo '=======' $POST_VERSION '======='
    # patch version in setup.py
    - sed -i "s/^\(\s\s*version=version\),$/\1 + '${POST_VERSION}',/"
          */setup.py modules/*/setup.py
    # build wheels
    - sed -i 's/ sdist / bdist_wheel /' .gitlab-scripts/generate-packages.sh
    - .gitlab-scripts/generate-packages.sh ${CI_PROJECT_DIR}/packages > /dev/null
    - ls ${CI_PROJECT_DIR}/packages/  # to ease finding issues

deploy:
  stage: deploy
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:slim
  rules:
    # only upload if either scheduled or run manually
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: on_success
    - when: manual
  before_script:
    - pip install twine
  script:
    - ls ${CI_PROJECT_DIR}/packages/  # to ease finding issues
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token
      twine upload
      --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi
      ${CI_PROJECT_DIR}/packages/*
