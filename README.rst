========
trydevpi
========

*Provide Python Packages of the Tryton in-development Version*

When working on a Tryton module on the in-development version
(mercurial's *default* branch)
one might want to run tests locally prior to pushing.
Running tests is done via ``tox``,
which tries to pull in-development versions of the packages.
*trydevpi* provides these packages.

Use it like this::

  CI_API_V4_URL=https://foss.heptapod.net/api/v4
  PIP_INDEX_URL=${CI_API_V4_URL}/projects/1411/packages/pypi/simple
  export PIP_INDEX_URL

  tox -e py311-sqlite


Package Availability
====================

For now, packages are build once a week.
If need arises, we will increase the schedule.
